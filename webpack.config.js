const path = require('path');
const webpack = require('webpack');

module.exports = {
	context: path.resolve(__dirname, "src"),
    entry: {
        index: './index.js'
    },
    output: {
        path: path.join(__dirname, './dist'),
        //filename: '[name].js',
        filename: 'bundle.js',

    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components|public)/,
            use: "babel-loader"
        }, {
            test: /\.json$/,
            use: 'json-loader'
        }, {
	        test: /\.svg$/,
	        use: 'svg-loader?pngScale=2'
        }, {
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        modules: [path.resolve(__dirname, "src"), 'node_modules'],
        extensions: ['.json', '.css', '.js'],
	    alias: {
		    components: path.resolve(__dirname, 'src/app/components/'),
		    templates: path.resolve(__dirname, 'src/app/templates/'),
		    services: path.resolve(__dirname, 'src/app/services/')
	    }
    },
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery'
		})
	],
	devtool: 'source-map'
};